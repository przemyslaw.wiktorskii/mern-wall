const mongoose = require('mongoose');
const { Schema } = mongoose;

const UserSchema = new Schema({
  googleID: String,
  name: String,
  lastname: String,
  color: String,
  createdacc: Date
});

mongoose.model('users', UserSchema);
