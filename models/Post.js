const mongoose = require('mongoose');
const { Schema } = mongoose;

const PostSchema = new Schema({
  author: { type: Schema.Types.ObjectId, ref: 'users' },
  body: String,
  dateSent: Date,
  ogtags: [
    {
      url: String,
      title: String,
      description: String,
      img: String
    }
  ],
  images: []
});

mongoose.model('post', PostSchema);
