const mongoose = require("mongoose");
const { Schema } = mongoose;

const CommentSchema = new Schema({
  author: { type: Schema.Types.ObjectId, ref: "users" },
  body: String,
  dateSent: Date
});

mongoose.model("comment", CommentSchema);
