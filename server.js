const express = require('express');
const mongoose = require('mongoose');
const app = express();
const passport = require('passport');
const bodyParser = require('body-parser');
const cookieSession = require('cookie-session');
const PORT = process.env.PORT || 5000;
const server = app.listen(PORT, () => {
  console.log(`Server runing on port ${PORT}`);
});
const io = require('socket.io')(server);
const keys = require('./config/keys');
require('./connection/awsConnection');
require('./models/User');
require('./models/Post');
require('./connection/socket')(io);
require('./services/passport');
// elo elo 3 2 0
mongoose.connect(
  keys.mongo.uri,
  { useNewUrlParser: true }
);

const db = mongoose.connection;

db.once('open', () => {
  console.log('Database connected');
});

db.once('error', err => {
  console.log('ERROR', err);
});
// wyrzucic przed deployem
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  next();
});

app.use(bodyParser.json());

app.use(
  cookieSession({
    maxAge: 30 * 24 * 60 * 60 * 1000,
    keys: [keys.cookieKey]
  })
);

app.use(passport.initialize());
app.use(passport.session());

require('./routes/authRoutes')(app);
require('./routes/userRoutes')(app);
require('./routes/imgRoutes')(app);
