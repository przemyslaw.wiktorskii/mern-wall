const { getlinks } = require('../services/helpers');
const { getOgsTags } = require('../services/ogscraper');
const {
  allPosts,
  savePost,
  editPost,
  deletePost
} = require('../repository/postrepo');
module.exports = io => {
  io.on('connection', socket => {
    // db actions

    socket.on('all_posts', data => {
      allPosts().then(posts => {
        io.sockets.emit('all_posts', posts);
      });
    });

    socket.on('new_post', data => {
      if (!data.author) {
        io.sockets.emit('new_post', { status: 401 });
      } else {
        const links = getlinks(data.message);

        if (links) {
          getOgsTags(links).then(ogsdata => {
            savePost(data, ogsdata).then(newpost => {
              io.sockets.emit('new_post', newpost);
            });
          });
        } else {
          savePost(data).then(newpost => {
            io.sockets.emit('new_post', newpost);
          });
        }
      }
    });

    socket.on('edit_post', data => {
      editPost(data).then(editedPost => {
        io.sockets.emit('edited_post', editedPost);
      });
    });

    socket.on('delete_post', data => {
      deletePost(data).then(deleted => {
        io.sockets.emit('delete_post', deleted);
      });
    });

    socket.on('disconnect', () => {
      console.log('User disconnect');
    });
  });
};
