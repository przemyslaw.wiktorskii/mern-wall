const AWS = require('aws-sdk');

AWS.config.loadFromPath('awsconfig.json');

const BUCKER_NAME = 'mernStackBucket';


const awsS3 = new AWS.S3();

const bucketConfig = { Bucket: BUCKER_NAME };

const checkIfBucketExist = error => {
  if (error) {
    console.log('could not create bucket');
  } else {
    console.log('AWS connection ready');
  }
};

awsS3.headBucket(bucketConfig, (error, data) => {
  if (error) {
    awsS3.createBucket(bucketConfig, (error, data) => {
      if (error) {
        checkIfBucketExist(error, null);
      } else {
        checkIfBucketExist(null, data);
      }
    });
  } else {
    checkIfBucketExist(null, data);
  }
});
