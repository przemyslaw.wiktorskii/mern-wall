FROM node:10

WORKDIR /var/www/

COPY package*.json /var/www/

RUN npm install

COPY . /var/www/

EXPOSE 5000

CMD ["npm" , "start"]