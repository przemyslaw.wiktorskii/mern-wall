const mongoose = require('mongoose');
const User = mongoose.model('users');
const Post = mongoose.model('post');

module.exports = {
  getUserInfo: userid => {
    return new Promise((resolve, reject) => {
      Post.find({ author: userid }).then((posts, error) => {
        if (error) {
          reject(error);
        }
        resolve(posts);
      });
    });
  }
};
