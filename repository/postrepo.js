const mongoose = require('mongoose');
const Post = mongoose.model('post');

module.exports = {
  savePost: (data, ogtags) => {
    return new Promise((resolve, reject) => {
      new Post({
        body: data.message,
        author: data.author,
        dateSent: data.date,
        ogtags: ogtags,
        images: [data.images]
      }).save((err, newpost) => {
        if (err) reject(err);
        Post.findById(newpost._id)
          .populate('author')
          .exec()
          .then(newpost => {
            resolve(newpost);
          });
      });
    });
  },
  allPosts: () => {
    return new Promise(resolve => {
      Post.find({})
        .populate('author')
        .sort({ dateSent: 'descending' })
        .exec()
        .then(posts => {
          resolve(posts);
        });
    });
  },
  editPost: data => {
    return new Promise((resolve, reject) => {
      Post.findOneAndUpdate(
        { _id: data.postID },
        { $set: { body: data.message } },
        { new: true },
        (error, editedPost) => {
          if (error) return reject(error);
          else {
            resolve(editedPost);
          }
        }
      );
    });
  },
  deletePost: data => {
    return new Promise((resolve, reject) => {
      Post.findByIdAndRemove({ _id: data }, (err, todo) => {
        if (err) reject(err);
        console.log('Post removed !');
        resolve(data);
      });
    });
  }
};
