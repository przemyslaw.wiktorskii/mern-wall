const { getUserInfo } = require('../repository/userRepo');
module.exports = app => {
  app.get('/api/profile/:id', (req, res) => {
    getUserInfo(req.params.id).then(posts => {
      res.json(posts);
    });
  });
};
