const formidable = require('formidable');
const AWS = require('aws-sdk');
const awsS3 = new AWS.S3();
const uuidv4 = require('uuid/v4');
const fs = require('fs');
const BUCKET_NAME = 'mernStackBucket';

module.exports = app => {
  app.post('/api/file-upload', (req, res) => {
    let form = new formidable.IncomingForm();
    form.parse(req, function(error, fields, files) {
      const fileKeys = Object.keys(files);
      const filesCount = fileKeys.length;
      let filesProcessed = 0;
      const uploadKeys = [];
      fileKeys.forEach(fileKey => {
        let file = files[fileKey];
        const key = uuidv4();

        uploadKeys.push(key);

        awsS3.upload(
          {
            Bucket: BUCKET_NAME,
            Key: key,
            Body: fs.createReadStream(file.path)
          },
          function(error, data) {
            if (error) {
              console.log(error);
            }

            filesProcessed += 1;

            if (filesProcessed === filesCount) {
              res.end(
                JSON.stringify({
                  files: uploadKeys
                })
              );
            }
          }
        );
      });
    });
  });

  app.get('/api/file/:fileId', function(req, res) {
    console.log(BUCKET_NAME);
    const params = {
      Bucket: BUCKET_NAME,
      Key: req.params.fileId
    };

    awsS3.headObject(params, function(err, data) {
      if (err) {
        res.end('file not found');
      } else {
        awsS3
          .getObject(params)
          .createReadStream()
          .pipe(res);
      }
    });
  });
};
