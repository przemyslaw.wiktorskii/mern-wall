import { FETCH_USER, FETCH_IMAGE_ID } from './types';
import axios from 'axios';
import socketIoClient from 'socket.io-client';
import FormData from 'form-data';
const socket = socketIoClient('http://localhost:5000');

export const fetchUser = () => async dispatch => {
  const res = await axios.get('/api/current_user');
  dispatch({ type: FETCH_USER, payload: res });
};

export const writePost = (post, images) => async dispatch => {
  console.log(post, images);
  socket.emit('new_post', {
    message: post.message,
    date: post.date,
    author: post.author,
    images: images
  });
};

export const writeComment = comment => async dispatch => {
  console.log({ comment });
};

export const sendImage = data => async dispatch => {
  const formData = new FormData();

  for (let i = 0; i < data.length; i++) {
    formData.append(data[i].name, data[i]);
  }

  const config = {
    headers: { 'content-type': 'multipart/form-data' }
  };

  axios.post('/api/file-upload', formData, config).then(response => {
    const res = response.data.files;
    dispatch({ type: FETCH_IMAGE_ID, payload: res });
  });
};
