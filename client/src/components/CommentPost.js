import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import { connect } from 'react-redux';
import { writeComment } from '../action/index';
class CommentPost extends Component {
  state = {
    message: '',
    date: '',
    author: ''
  };

  handleChange = event => {
    const { _id: author_id } = this.props.auth.data;
    const value = event.target.value;

    this.setState({
      message: value,
      date: new Date(),
      author: author_id
    });
  };

  handleKeyPress = event => {
    if (event.key === 'Enter') {
      this.props.writeComment(this.state);
    }
  };

  render() {
    return (
      <div>
        <TextField
          onChange={this.handleChange}
          onKeyPress={this.handleKeyPress}
          id="edit-width"
          label="Add comment"
          placeholder="Write comment"
          fullWidth
        />
      </div>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  return { auth };
};

const mapDispatchToProps = dispatch => {
  return {
    writeComment: comment => dispatch(writeComment(comment))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CommentPost);
