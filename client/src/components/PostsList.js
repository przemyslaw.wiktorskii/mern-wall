import React, { Component } from 'react';

import Grid from '@material-ui/core/Grid';

import Post from './Post';

class Listposts extends Component {
  constructor() {
    super();
    this.state = {
      posts: []
    };
  }
  componentDidMount() {
    this.fetchPostsSocket();
    const { socket } = this.props;
    socket.on('new_post', post => {
      if (post.status !== 401) {
        this.state.posts.unshift(post);
        this.forceUpdate();
      }
    });

    socket.on('edited_post', post => {
      this.onTitleChange(post._id, post.body);
    });

    socket.on('delete_post', data => {
      this.onDeletingPost(data);
    });
  }

  onTitleChange = (id, updatedBody) => {
    let data = [...this.state.posts];

    let index = data.findIndex(obj => obj._id === id);
    data[index].body = updatedBody;

    const updateBodySpan = document.getElementById(`${data[index]._id}`);
    updateBodySpan.textContent = updatedBody;
    this.setState({ posts: data });
  };

  onDeletingPost = id => {
    const post = document.getElementById(`post${id}`);
    post.remove();
  };

  renderPosts() {
    switch (this.state.posts) {
      case []:
        return;
      default:
        return (
          this.state.posts &&
          this.state.posts.map(post => (
            <Post key={post._id} data={post} socket={this.props.socket} />
          ))
        );
    }
  }

  fetchPostsSocket() {
    const { socket } = this.props;
    socket.emit('all_posts', 'fetch_posts');
    socket.on('all_posts', posts => {
      this.setState({ posts });
    });
  }
  render() {
    return (
      <div>
        <Grid
          container
          spacing={24}
          style={{
            display: 'flex',
            justifyContent: 'center',
            marginTop: '25px'
          }}
        >
          <Grid item sm={3} md={6} lg={6}>
            {this.renderPosts()}
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default Listposts;
