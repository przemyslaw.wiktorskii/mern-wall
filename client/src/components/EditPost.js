import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";

class EditPost extends Component {
  handleKeyPress = e => {
    if (e.key === "Enter") {
      this.props.editAction(e.target.value);
    }
  };

  render() {
    return (
      <div>
        <TextField
          onKeyPress={this.handleKeyPress}
          id="edit-width"
          label="Edit post"
          placeholder="Write some text..."
          fullWidth
          margin="normal"
        />
      </div>
    );
  }
}

export default EditPost;
