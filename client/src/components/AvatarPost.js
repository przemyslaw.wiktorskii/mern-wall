import React from 'react';
import Avatar from '@material-ui/core/Avatar';

const AvatarPost = props => {
  return (
    <Avatar aria-label="Recipe" style={{ backgroundColor: props.color }}>
      {props.initials.toUpperCase()}
    </Avatar>
  );
};

export default AvatarPost;
