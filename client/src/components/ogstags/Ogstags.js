import React from 'react';

const Ogstags = props => {
  return (
    <div style={{ margin: '10px' }}>
      <h2>{props.data.title}</h2>
      <img
        style={{ maxHeight: '300px', borderRadius: '5px' }}
        src={props.data.img}
        alt=""
      />
      <p style={{ fontWeight: 'bold' }}>{props.data.description}</p>
    </div>
  );
};

export default Ogstags;
