import React from 'react';

const Images = props => {
  if (props.data.length !== 0) {
    return (
      <div style={{ margin: '10px' }}>
        {props.data.map(img => (
          <img
            key={img}
            style={{ maxHeight: '300px', borderRadius: '5px' }}
            src={`/api/file/${img}`}
            alt="imgpost"
          />
        ))}
      </div>
    );
  } else {
    return <div />;
  }
};

export default Images;
