import React, { Component } from 'react';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import EditPost from './EditPost';
class PostOptions extends Component {
  state = {
    open: false
  };

  onEditHandle() {
    this.setState({ open: !this.state.open });
  }

  editPostAction = value => {
    if (value) {
      const socket = this.props.socket;
      let editedPost = {
        postID: this.props.data,
        message: value,
        date: new Date()
      };
      socket.emit('edit_post', editedPost);
    }
  };
  deletePost = () => {
    const socket = this.props.socket;
    socket.emit('delete_post', this.props.data);
  };
  render() {
    return (
      <div>
        <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
          <CardActions>
            <Button
              onClick={() => this.onEditHandle()}
              size="small"
              color="primary"
              variant="contained"
            >
              Edit
            </Button>
            <Button
              onClick={this.deletePost}
              size="small"
              variant="contained"
              color="secondary"
            >
              Delete
            </Button>
          </CardActions>
        </div>
        {this.state.open && <EditPost editAction={this.editPostAction} />}
      </div>
    );
  }
}

export default PostOptions;
