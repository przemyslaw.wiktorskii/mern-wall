import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import Linkify from 'react-linkify';
import { withStyles } from '@material-ui/core/styles';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import Collapse from '@material-ui/core/Collapse';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import PostOptions from './PostOptions';
import CommentPost from './CommentPost';
import AvatarPost from './AvatarPost';
import classnames from 'classnames';
import Ogstags from './ogstags/Ogstags';
import Images from './images/Images';

const styles = theme => ({
  cardGrid: {
    padding: `${theme.spacing.unit * 8}px 0`
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  cardMedia: {
    paddingTop: '56.25%' // 16:9
  },

  cardContent: {
    flexGrow: 1
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest
    }),
    marginLeft: 'auto',
    [theme.breakpoints.up('sm')]: {
      marginRight: -8
    }
  },
  avatar: {
    backgroundColor: 'red'
  },
  expandOpen: {
    transform: 'rotate(180deg)'
  }
});

class Post extends Component {
  state = {
    addcomment: false,
    showOpt: false,
    open: false,
    expanded: false
  };
  renderOptionButtons() {
    let authName = this.props.auth.data.name + this.props.auth.data.lastname;
    let postName =
      this.props.data.author.name + this.props.data.author.lastname;
    if (authName === postName) {
      return (
        <IconButton onClick={this.showOptions}>
          <MoreVertIcon />
        </IconButton>
      );
    }
  }

  handleExpandClick = () => {
    this.setState(state => ({ expanded: !state.expanded }));
  };

  showOptions = () => {
    this.setState({ showOpt: !this.state.showOpt });
  };
  addCommentHandle = () => {
    if (this.props.auth.data !== '') {
      this.setState({ addcomment: !this.state.addcomment });
      document.querySelector('.commentLine').style.border = 'transparent';
      if (this.state.addcomment) {
        document.querySelector('.commentLine').style.borderBottom =
          '2px solid #3c52b0';
      }
    } else {
      // todo: add loginForm in these place !!!
      alert('LoginModal');
    }
  };

  userInitials = () => {
    return this.props.data.author.name[0];
  };

  renderIdPost() {
    return `post${this.props.data._id}`;
  }

  formatDate(date) {
    const dateobj = new Date(date);
    const formatedDate =
      dateobj.getFullYear() +
      '-' +
      ('0' + (dateobj.getMonth() + 1)).slice(-2) +
      '-' +
      ('0' + dateobj.getDate()).slice(-2) +
      '  ' +
      ('0' + dateobj.getHours()).slice(-2) +
      ':' +
      ('0' + dateobj.getMinutes()).slice(-2);
    return formatedDate;
  }

  // todo : Work on post view

  render() {
    const { classes } = this.props;
    const fullUserName = (
      <div style={{ fontFamily: 'Ubuntu, sans-serif', fontWeight: 'bold' }}>
        {this.props.data.author.name} {this.props.data.author.lastname}
      </div>
    );
    const postDate = this.formatDate(this.props.data.dateSent);
    return (
      <div style={{ marginTop: '20px' }} id={this.renderIdPost()}>
        <Card className={classes.card}>
          <CardHeader
            avatar={
              <AvatarPost
                initials={this.userInitials()}
                color={this.props.data.author.color}
              />
            }
            action={this.renderOptionButtons()}
            title={fullUserName}
            subheader={postDate}
          />
          <Collapse in={this.state.showOpt} timeout="auto" unmountOnExit>
            {this.state.showOpt && (
              <PostOptions
                data={this.props.data._id}
                auth={this.props.auth}
                socket={this.props.socket}
                deletePost={this.deletePost}
              />
            )}
          </Collapse>
          {this.props.data.ogtags.map(ogtag => (
            <Ogstags key={ogtag._id} data={ogtag} classes={classes} />
          ))}

          <CardContent className={classes.cardContent}>
            <Typography variant="headline">
              <p
                id={this.props.data._id}
                style={{ fontFamily: 'Ubuntu, sans-serif' }}
              >
                <Linkify>{this.props.data.body}</Linkify>
              </p>
              <div className="images-container">
                {this.props.data.images.map(img => (
                  <Images key={img} data={img} />
                ))}
              </div>
            </Typography>
          </CardContent>
          <CardActions>
            <IconButton
              className={classnames(classes.expand, {
                [classes.expandOpen]: this.state.expanded
              })}
              onClick={this.handleExpandClick}
              aria-expanded={this.state.expanded}
              aria-label="Show more"
            >
              <ExpandMoreIcon />
            </IconButton>

            <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
              <div
                className="commentLine"
                style={{
                  borderBottom: '2px solid #3c52b0',
                  paddingBottom: '20px'
                }}
              >
                <Button
                  onClick={this.addCommentHandle}
                  size="small"
                  color="primary"
                  variant="contained"
                >
                  Comment
                </Button>
                {this.state.addcomment && <CommentPost />}
              </div>

              <CardContent>
                <Typography paragraph>Comments:</Typography>
                <Typography paragraph>
                  Heat 1/2 cup of the broth in a pot until simmering, add
                  saffron and set aside for 10 minutes.
                </Typography>
                <Typography paragraph>
                  Heat oil in a (14- to 16-inch) paella pan or a large, deep
                  skillet over medium-high heat. Add chicken, shrimp and
                  chorizo, and cook, stirring occasionally until lightly
                  browned, 6 to 8 minutes. Transfer shrimp to a large plate and
                  set aside, leaving chicken and chorizo in the pan. Add
                  pimentón, bay leaves, garlic, tomatoes, onion, salt and
                  pepper, and cook, stirring often until thickened and fragrant,
                  about 10 minutes. Add saffron broth and remaining 4 1/2 cups
                  chicken broth; bring to a boil.
                </Typography>

                <Typography>
                  Set aside off of the heat to let rest for 10 minutes, and then
                  serve.
                </Typography>
              </CardContent>
            </Collapse>
          </CardActions>
        </Card>
      </div>
    );
  }
}

function mapStateToProps({ auth }) {
  return { auth };
}

export default compose(
  withStyles(styles),
  connect(mapStateToProps)
)(Post);
