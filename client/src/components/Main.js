import React, { Component } from 'react';
import socketIoClient from 'socket.io-client';
import { connect } from 'react-redux';
import Addpost from './Addpost';
import PostList from './PostsList';

import Grid from '@material-ui/core/Grid';

class Main extends Component {
  constructor() {
    super();
    this.state = {
      socket: socketIoClient('http://localhost:5000')
    };
  }

  render() {
    return (
      <div className="Main">
        <Grid item xs={12}>
          <h1>Hello user write some posts</h1>
          <hr />
          <Addpost socket={this.state.socket} />
          <div style={{ width: '65%', margin: '0 auto' }}>
            <PostList socket={this.state.socket} />
          </div>
        </Grid>
      </div>
    );
  }
}

function mapStateToProps({ auth }) {
  return { auth };
}

export default connect(mapStateToProps)(Main);
