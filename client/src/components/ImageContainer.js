import React from 'react';

const Imagecontainer = props => {
  console.log(props);
  return (
    <div
      style={{
        width: '50px',
        height: '50px'
      }}
    >
      <img src={'/file/' + props.data} alt="" />
      <h4>{props.title}</h4>
    </div>
  );
};

export default Imagecontainer;
