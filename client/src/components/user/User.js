import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import UserPostList from './UserPostList';
class User extends Component {
  state = {
    userPosts: [],
    renderPosts: false
  };
  componentDidMount() {
    const url = window.location.href;
    const urltocut = url.indexOf('/profile/');
    const req = url.substr(urltocut);

    fetch(`http://localhost:5000/api${req}`)
      .then(data => data.json())
      .then(data => {
        this.setState({ userPosts: data });
      });
  }

  showAllPosts = () => {
    this.setState({ renderPosts: !this.state.renderPosts });
  };

  render() {
    return (
      <div className="user-profile-section">
        <h1>You write {this.state.userPosts.length} posts</h1>
        <Button
          onClick={this.showAllPosts}
          color="primary"
          variant="contained"
          style={{ textTransform: 'capitalize' }}
        >
          Show all my posts
        </Button>
        <div className="post-list">
          {this.state.renderPosts && (
            <UserPostList data={this.state.userPosts} />
          )}
        </div>
      </div>
    );
  }
}

export default User;
