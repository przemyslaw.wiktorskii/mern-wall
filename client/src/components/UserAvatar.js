import React from "react";
import { connect } from "react-redux";
import Avatar from "@material-ui/core/Avatar";

const UserAvatar = props => {
  const { name, lastname, color } = props.auth.data;
  if (name && lastname) {
    const initials = name[0];

    return (
      <div>
        <Avatar aria-label="Recipe" style={{ backgroundColor: color }}>
          {initials.toUpperCase()}
        </Avatar>
      </div>
    );
  }
};

const mapStateToProps = state => {
  return state;
};

export default connect(mapStateToProps)(UserAvatar);
