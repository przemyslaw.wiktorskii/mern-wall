import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';

import LoginModal from './LoginModal';
import { writePost, sendImage } from '../action/index';

function getModalStyle() {
  const top = 25;
  const left = 45;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`
  };
}

const styles = theme => ({
  paper: {
    textAlign: 'center',
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4
  },
  register: {}
});

class Addpost extends Component {
  state = {
    open: false,
    googleID: '',
    images: [],
    post: {
      message: '',
      date: '',
      author: ''
    }
  };

  componentDidMount() {
    const { socket } = this.props;
    socket.on('new_post', post => {
      if (post.status === 401) {
        this.setState({ open: true });
      }
    });
  }

  handleChangeInput = event => {
    const value = event.target.value;
    this.setState(pervState => ({
      ...pervState,
      post: {
        message: value,
        date: new Date(),
        author: this.props.auth.data._id
      }
    }));
  };

  handleKeyPress = event => {
    if (event.key === 'Enter') {
      this.props.writePost(this.state.post, this.state.images);
      event.target.value = '';
      this.setState({ images: [] });
      event.preventDefault();
    }
  };

  componentWillReceiveProps(nextProps) {
    if (Object.keys(nextProps.posts).length !== 0) {
      nextProps.posts.map(postid => {
        return this.setState({ images: [...this.state.images, postid] });
      });
    }
  }

  handleClose = () => {
    this.setState({ open: false });
  };

  deleteAddingImg = e => {
    const imgId = e.target.getAttribute('data-id');
    const newimgarr = this.state.images.filter(img => img !== imgId);
    this.setState({ images: newimgarr });
  };

  renderImgs = () => {
    if (this.state.images.length !== 0) {
      return this.state.images.map(img => (
        <img
          onClick={this.deleteAddingImg}
          key={img}
          data-id={img}
          src={`/api/file/${img}`}
          alt="postimg"
        />
      ));
    }
  };

  handleDragOver = e => {
    const file = e.dataTransfer.files;
    this.props.sendImage(file);
    e.preventDefault();
  };

  render() {
    const { classes } = this.props;
    this.renderImgs();
    return (
      <div style={{ display: 'flex', justifyContent: 'center' }}>
        <Grid item xs={6}>
          <Typography gutterBottom />
          <form action="">
            <div onDrop={this.handleDragOver} className="container-drag">
              <TextField
                onChange={this.handleChangeInput}
                onKeyPress={this.handleKeyPress}
                id="full-width"
                label="Add post"
                placeholder="Write some text..."
                fullWidth
                margin="normal"
              />
            </div>
          </form>
          <div className="imgscontainer">{this.renderImgs()}</div>
          <LoginModal
            open={this.state.open}
            onClose={this.handleClose}
            getModalStyle={getModalStyle}
            classes={classes}
          />
        </Grid>
      </div>
    );
  }
}

function mapStateToProps({ auth, posts }) {
  return { auth, posts };
}

const mapDispatchToProps = dispatch => {
  return {
    writePost: (post, images) => dispatch(writePost(post, images)),
    sendImage: images => dispatch(sendImage(images))
  };
};

const Addposts = withStyles(styles)(Addpost);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Addposts);
