import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { compose } from 'recompose';
import { withStyles } from '@material-ui/core/styles';
import UserAvatar from './UserAvatar';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Fade from '@material-ui/core/Fade';

const styles = {
  root: {
    flexGrow: 1
  },
  flex: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  link: {
    textDecoration: 'none',
    color: 'white'
  }
};

class Header extends Component {
  state = {
    anchorEl: null
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  renderContent() {
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);

    if (this.props.auth == null) {
      return;
    }
    switch (this.props.auth.data) {
      case '':
        return (
          <Button
            color="primary"
            variant="contained"
            style={{ textTransform: 'capitalize' }}
            href="/auth/google"
          >
            Login with google
          </Button>
        );
      default:
        return [
          <p style={{ margin: '0 5px 5px' }}>Logged in as:</p>,
          <UserAvatar />,
          <Button
            style={{ color: 'white', textTransform: 'capitalize' }}
            aria-owns={open ? 'fade-menu' : null}
            aria-haspopup="true"
            onClick={this.handleClick}
          >
            {this.props.auth.data.name} {this.props.auth.data.lastname}
          </Button>,
          <Menu
            id="fade-menu"
            anchorEl={anchorEl}
            open={open}
            onClose={this.handleClose}
            TransitionComponent={Fade}
          >
            <MenuItem onClick={this.handleClose}>
              <Link
                style={{ textDecoration: 'none' }}
                to={'/profile/' + this.props.auth.data._id}
              >
                My profile
              </Link>
            </MenuItem>
            <a href="/api/logout" style={{ textDecoration: 'none' }}>
              <MenuItem onClick={this.handleClose}>Logout</MenuItem>
            </a>
          </Menu>
        ];
    }
  }
  render() {
    const { classes } = this.props;
    return (
      <div>
        <div className={classes.root}>
          <AppBar position="fixed">
            <Toolbar>
              <Typography
                style={{ textAlign: 'left', marginLeft: '50px' }}
                variant="title"
                color="inherit"
                className={classes.flex}
              >
                <Link className={classes.link} to={'/'}>
                  Mern Wall
                </Link>
              </Typography>

              {this.renderContent()}
            </Toolbar>
          </AppBar>
        </div>
      </div>
    );
  }
}

function mapStateToProps({ auth }) {
  return { auth };
}

export default compose(
  withStyles(styles),
  connect(mapStateToProps)
)(Header);
