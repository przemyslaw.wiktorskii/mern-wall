import { FETCH_POSTS, FETCH_IMAGE_ID } from '../action/types';
export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_POSTS:
      return action.payload;
    case FETCH_IMAGE_ID: 
      return action.payload;
    default:
      return state;
  }
}
