import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from './action';
import Header from './components/Header';
import Main from './components/Main';
import User from './components/user/User';
import Footer from './components/Footer';
import './App.css';

class App extends Component {
  componentDidMount() {
    this.props.fetchUser();
  }
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <div>
            <Header />
            <div>
              <Route exact path="/" component={Main} />
              <Route exact path="/profile/:id" component={User} />
            </div>
            <Footer />
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default connect(
  null,
  actions
)(App);
