module.exports = {
  getlinks: message => {
    return message.match(/https?:\/\/[^\s]+/g);
  },

  addHttp: url => {
    const http = 'http://',
      https = 'https://';

    if (url !== null) {
      if (
        url.substr(0, http.length) !== http &&
        url.substr(0, https.length) !== https
      )
        url = http + url ? https + url : http + url;
    }

    return url;
  }
};
