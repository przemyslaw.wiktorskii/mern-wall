const passport = require('passport');
const mongoose = require('mongoose');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const keys = require('../config/keys');

const User = mongoose.model('users');

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  User.findById(id).then(user => {
    done(null, user);
  });
});

const getRandomColor = () => {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
};

passport.use(
  new GoogleStrategy(
    {
      clientID: keys.googleAuth.appID,
      clientSecret: keys.googleAuth.appSecret,
      callbackURL: '/auth/google/callback',
      proxy: true
    },
    async (accessToken, refreshToken, profile, done) => {
      const findedUser = await User.findOne({ googleID: profile.id });

      if (findedUser) {
        return done(null, findedUser);
      }

      const user = await new User({
        googleID: profile.id,
        name: profile.name.givenName,
        lastname: profile.name.familyName,
        color: getRandomColor(),
        createdacc: Date.now()
      }).save();
      done(null, user);
    }
  )
);
