const { addHttp } = require('./helpers');
const ogs = require('open-graph-scraper');
module.exports = {
  getOgsTags: links => {
    return new Promise((resolve, reject) => {
      const ogsdata = [];

      links.map(link => {
        ogs(
          {
            url: link
          },
          (error, results) => {
            if (error) reject(error);
            else {
              if (results.data.ogImage.url === undefined) {
                results.data.ogImage.url = null;
              }
              ogsdata.push({
                url: results.data.ogUrl,
                title: results.data.ogTitle,
                description: results.data.ogDescription,
                img: addHttp(results.data.ogImage.url)
              });
            }
            if (ogsdata.length === links.length) {
              return resolve(ogsdata);
            }
          }
        );
      });
    });
  }
};
